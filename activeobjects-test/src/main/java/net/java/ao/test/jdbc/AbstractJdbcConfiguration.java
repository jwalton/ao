package net.java.ao.test.jdbc;

import com.google.common.base.Objects;

/**
 *
 */
public abstract class AbstractJdbcConfiguration implements JdbcConfiguration
{
    public String getUsername()
    {
        return "ao_user";
    }

    public String getPassword()
    {
        return "ao_password";
    }

    @Override
    public String getSchema()
    {
        return null;
    }

    @Override
    public final int hashCode()
    {
        return Objects.hashCode(getUsername(), getPassword(), getUrl(), getSchema());
    }

    @Override
    public final boolean equals(Object obj)
    {
        if (obj == null)
        {
            return false;
        }
        if (obj == this)
        {
            return true;
        }
        if (!(obj instanceof JdbcConfiguration))
        {
            return false;
        }

        final JdbcConfiguration that = (JdbcConfiguration) obj;
        return this.getUsername().equals(that.getUsername())
                && this.getPassword().equals(that.getPassword())
                && this.getUrl().equals(that.getUrl())
                && Objects.equal(this.getSchema(), that.getSchema());
    }

    @Override
    public final String toString()
    {
        return new StringBuilder().append(getUrl())
                .append(" :: ").append(getSchema() != null ? getSchema() : "<no schema>")
                .append(" - ").append(getUsername())
                .append(" - ").append(getPassword())
                .toString();
    }
}
